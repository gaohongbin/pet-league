package cn.nonexistent.petleague;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetLeagueApplication {

	public static void main(String[] args) {
		SpringApplication.run(PetLeagueApplication.class, args);
	}

}
